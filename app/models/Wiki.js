export default class {

    constructor(object) {
        if (object.id) {
            this.id = object.id
        } else {
            this.id = Math.random().toString(36).substring(7)
        }
        this.language = object.language
        if (object.title !== undefined) { // Build an entry from an object (retrieve a stored item)
            this.title = object.title
            this.description = object.description
            this.images = object.images
            this.url = object.url
            this.saved = object.saved
            this.state = "loaded"
        } else {
            this.state = "waiting"
        }
    }
}