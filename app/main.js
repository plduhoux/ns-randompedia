import Vue from 'nativescript-vue'
import App from './components/App'
import VueDevtools from 'nativescript-vue-devtools'
import RadListView from 'nativescript-ui-listview/vue'
import RadSideDrawer from 'nativescript-ui-sidedrawer/vue'
//import store from './store';
import wikiService from "@/helpers/WikiService"
import {TNSFontIcon, fonticon} from 'nativescript-fonticon';

// Use VueDevTools
if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
}

// Use RadListView, RadSideDrawer (loaded from just importing module)
Vue.use(RadListView)
//Photo gallery element --> use it in Vue
Vue.registerElement("ImageSwipe", () => require("nativescript-image-swipe/image-swipe").ImageSwipe);
//Ripple element to add ripple to label buttons
Vue.registerElement("Ripple", () => require("nativescript-ripple").Ripple);

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')

//Initialize FontAwesome
TNSFontIcon.debug = true;
TNSFontIcon.paths = {
  'fa': './fonts/FontAwesome.css'
};
TNSFontIcon.loadCss();
Vue.filter('fonticon', fonticon);

wikiService.load()
//.then(() => {
  new Vue({
      render: h => h('frame', [h(App)])
    }
  ).$start()
//})
