import cheerio from "cheerio-without-node-native"
import md5 from "js-md5"

import * as utils from "./utils"

export default class DomParser {
    constructor(dom, item) {
        this.dom = dom;
        this.item = item;

        this.$ = cheerio.load(dom)
    }

    /**
     * Get wikipedia entry description
     * @param {} dom 
     */
    getDescription() {
        // sometimes, there are style tags inside p tags, remove it or we will have css code in extract
        this.$(".mw-parser-output .mf-section-0 > p style").remove()
        // remove references to avoid [1], [2] in extract
        this.$(".mw-parser-output .mf-section-0 > p .reference").remove()
        // return the text of direct paragraphs from first section
        return this.$(".mw-parser-output .mf-section-0 > p").text().trim()
    }

    checkImageUrl(url) {
        if (url === undefined) return url
        if (url.indexOf("//") === 0) url = "https:" + url;
        if (url.indexOf("/static") === 0)
            url = utils.getWikipediaHostUrl(this.item.language) + url;
        return url
    }
    /**
     * Retrieve list of images urls from wikipedia entry
     * @param {} dom 
     */
    getImagesSrc() {
        let imgsbestresol = []
        let self = this
        this.$("a.image").each(function() {
            let $elem = self.$(this)
            let domi = $elem.children("img")
            let maxsizedurl, 
                thumburl = self.checkImageUrl(domi.attr("src"))

            // If img.attr("href") is /wiki/File|Fichier|Datei|...:name_of_the_file
            // possible to access original file using https://upload.wikimedia.org/wikipedia/commons/ + first letter of md5(name_of_the_file) + / first two letters of md5 / name_of_file
            let regex_fname = /wiki\/([^\:]+):([^\?]*)/g
            let match = regex_fname.exec($elem.attr("href"))
            if (false && match !== undefined) { // get original file
                let fname = match[2]
                let hash = md5(decodeURI(fname))
                // this is not the right url everytime... for language en, need to request api to get real url. Sample :
                // https://en.wikipedia.org/w/api.php?action=query&format=json&prop=imageinfo&titles=File%3ANJFair-horse%2Ejpg&iiprop=url%7Csize%7Cmime
                let url = "https://upload.wikimedia.org/wikipedia/commons/" + hash.substr(0, 1) + "/" + hash.substr(0, 2) + "/" + fname
                maxsizedurl = url
            } else {
                if (domi.attr("srcset")) {
                    // Get the best resol from srcset
                    let sset = domi.attr("srcset").split(",")
                    // best is always the last
                    maxsizedurl = self.checkImageUrl(sset[sset.length - 1].trim().split(" ")[0])
                }
            }
            imgsbestresol.push({
                thumb: thumburl,
                original: maxsizedurl !== undefined ? maxsizedurl : thumburl
            })
        })

        return imgsbestresol
    }

    /**
     * Get wikipedia entry url
     * @param {*} dom 
     */
    getUrl() {
        let pagename_regex = /wgPageName":\s*"((?:[^"\\]|\\.)*)"/gmu;
        let match = pagename_regex.exec(this.dom);
        if (match !== null) {
            return utils.getWikipediaBaseUrl(this.item.language) + match[1];
        }
        return null
    }

    /**
     * Get wikipedia entry title
     * @param {*} dom 
     */
    getTitle() {
        let title_regex = /wgTitle":\s*"((?:[^"\\]|\\.)*)"/gmu;
        let match = title_regex.exec(this.dom);

        if (match !== null) {
            return match[1]
        }
        return null
    }
}