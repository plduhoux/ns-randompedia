import localStorage from "nativescript-localstorage/localstorage"
import Wiki from "@/models/Wiki"
import * as utils from "@/helpers/utils"
import { device } from "tns-core-modules/platform";
import {languages} from "@/helpers/languages"
import messageService from "@/helpers/MessageService"
import DomParser from "./DomParser";

/**
 * Store the state of the app, save it to localStorage and contains actions to update the state
 */
class WikiService {
    constructor() {
        // localStorage.clear(); // to test with a fresh new app :)
        this.wikis = [] // list of all items
        this.ids = [] // list of ids objects containing {id, saved}

        //parameters of the app
        this.nb = localStorage.getItem("nb") || 10
        this.language = localStorage.getItem("language") || device.language || "en"
        let langExist = languages.find(lng => lng.code === this.language)
        if (langExist === undefined) this.language = "en"

        messageService.display({
            id: "initlanguage",
            onlyonce: true,
            icon: "fa-language",
            text: "Your language has been set to " + langExist.name + (langExist.name != langExist.localname ? " (" + langExist.localname + ")": "") + ". You can change it in the settings page."
        })
    }

    /**
     * Actions to save and retrieve from storage
     */
    /**
     * Load the list from localStorage, if some items are missing, fill it
     */
    load() {
        let self = this
        // retrieve from localStorage
        return new Promise((resolve, reject) => {
            try {
                let lststr = localStorage.getItem("wikis")
                if (lststr !== null) {
                    let lst = JSON.parse(lststr)
                    self.wikis = lst.map(ent => new Wiki(ent))
                    self.ids = self.wikis.map(wiki => { return {id: wiki.id, saved: wiki.saved} })
                }
                if (self.wikis.length === 0) {
                    self.keepNumberOfItems()
                    .then(() => resolve([...this.wikis]))
                } else {
                    resolve([...this.wikis])
                }
            } catch (e) {
                console.log("Error while loading localStorage ", e)
                return Promise.resolve([])
            }
        })
    }
    /**
     * Save items to localStorage
     */
    save() {
        let str = JSON.stringify(
            this.wikis.map(item => {
                return {
                    id: item.id,
                    title: item.title,
                    description: item.description,
                    images: item.images,
                    url: item.url,
                    saved: item.saved,
                    state: item.state,
                    language: item.language
                }
            })
        )
        try {
            localStorage.setItem("wikis", str)
        } catch (e) { console.log("Error while saving to localStorage ", e) }
    }

    /**
     * Getters of the list and setters of parameters
     */
    /**
     * Return list of unsaved (temp) items
     */
    unsaved() {
        return this.wikis.filter(item => !item.saved)
    }
    /**
     * Return list of saved items
     */
    saved() {
        return this.wikis.filter(item => item.saved === true)
    }
    get(id) {
        return this.wikis.find(wiki => wiki.id === id)
    }
    /**
     * Change current language
     * @param {*} language 
     */
    setCurrentLanguage(language) {
        if (language === this.language) return;

        this.language = language
        localStorage.setItem("language", language)

        let langExist = languages.find(lng => lng.code === this.language)
        messageService.display({
            id: "setlanguage",
            onlyonce: false,
            replaceExisting: true,
            icon: "fa-language",
            text: "Your language has been set to " + langExist.name + (langExist.name != langExist.localname ? " (" + langExist.localname + ")": "") + ". To view random articles in this language, pull the list from the top to reload, or reload from the menu."
        })
    }
    /**
     * Define the number of temp items
     * @param {*} nb 
     */
    setNbDisplay(nb) {
        this.nb = nb
        localStorage.setItem("nb", nb)
    }

    /**
     * Mutations from the state
     */
    /**
     * Update an item in list
     * @param {*} item 
     */
    updateItem(item) {
        let itemToUpdate = this.wikis.find(i => i.id === item.id)
        Object.assign(itemToUpdate, item)
        this.save()
        return Promise.resolve(item)
    }
    /**
     * Add a new item to list of items
     * @param {*} item 
     */
    addItem(item) {
        this.wikis.push(item)
        this.save()
        this.ids.push({id: item.id, saved: item.saved})
        return Promise.resolve(item)
    }
    /**
     * Remove an item from list of items
     * @param {*} item 
     */
    deleteItem(item) {
        this.ids.splice(this.ids.findIndex(id => id === item.id), 1)
        this.wikis.splice(this.wikis.findIndex(i => i.id === item.id), 1)
        this.save()
        return Promise.resolve(item)
    }

    /**
     * Actions to mutate the state 
     */
    /**
     * Maintain the right number of items in list
     * @param {*} param0 
     */
    keepNumberOfItems() {
        // Build fresh new list
        let nb = this.nb
        let nbcur = this.unsaved().length
        if (nbcur < nb) {
            let addPs = []
            for (let i = 0; i < nb - nbcur; i++) {
                addPs.push(this.addRandomItem())
            }
            return Promise.all(addPs)
        } else if (nbcur > nb) {
            let itemList = this.unsaved()
            let todel = []
            for (let i = 1; i <= nbcur - nb; i++) {
                todel.push(itemList[itemList.length - i])
            }
            let self = this
            return Promise.all(todel.map(item => self.deleteItem(item)))
        } else {
            return Promise.resolve()
        }
    }
    /**
     * Add a random item to the list
     * @param {*} param0 
     */
    addRandomItem() {
        let self = this
        return new Promise((resolve, reject) => {
            let item = new Wiki({ language: self.language })
            self.addItem(item)
                .then(() => {
                    self.loadRandom(item)
                        .then(item => {
                            resolve(item)
                        })
                        .catch(error => {
                            console.error(`Error adding item: ${error}.`)
                            reject(error)
                        })
                })
        })
    }
    randomizeAll() {
        let items = this.unsaved()
        let self = this
        return Promise.all(
            items.map(item => self.randomizeItem(item))
        )
    }
    /**
     * Refresh an item with a new random value
     * @param {*} param0 
     * @param {*} item 
     */
    randomizeItem(item) {
        let self = this
        return new Promise((resolve, reject) => {
            self.loadRandom(item)
                .then(item => {
                    resolve(item)
                })
                .catch(error => {
                    console.error(`Error randomizing item: ${error}.`)
                    reject(error)
                })
        })
    }
    /**
     * Load a random page and update an item with its data
     * @param {*} param0 
     * @param {*} item 
     */
    loadRandom(item) {
        let self = this
        return new Promise((resolve, reject) => {
            self.updateItem({ ...item, state: "loading", language: self.language })
                .then(item => {
                    fetch(utils.getWikipediaRandomUrl(item.language))
                        .then(response => response.text())
                        .then(text => {
                            let parser = new DomParser(text, item)
                            let nprops = {
                                title: parser.getTitle(),
                                description: parser.getDescription(),
                                images: parser.getImagesSrc(),
                                url: parser.getUrl(),
                                state: "loaded"
                            }
                            self.updateItem({ ...item, ...nprops })
                            console.log("######### random loaded " + nprops.title)
                            resolve(item);
                        })
                        .catch((e) => {
                            // Error while parsing item or fetching item
                            self.updateItem({ ...item, state: "error" })
                            .then(() => resolve())
                        })
                });
        });
    }
    /**
     * Update an item images list
     * @param {*} param0 
     * @param {*} item 
     */
    updateItemImages({ item, images }) {
        return this.updateItem({ ...item, images: images, imagesLoaded: true })
    }
    /**
     * Save an item for later
     * @param {*} param0 
     * @param {*} item 
     */
    saveItemForLater(item) {
        let self = this
        return this.updateItem({ ...item, saved: true })
            .then(item => {
                self.ids.find(elt => elt.id === item.id).saved = true
                return Promise.resolve(item)
            }).then(
                self.keepNumberOfItems()
            )
    }
}
export default new WikiService()