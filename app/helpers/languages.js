/**
 * This is the list of existing wikipedias
 * Extracted from https://en.wikipedia.org/wiki/List_of_Wikipedias#Detailed_list
 * with the following js code : 
 * var langs = [];$(".wikitable:last tbody tr").each(function(i) {langs.push({code: $("td:nth-child(3)", $(this)).text().trim(), name: $("td:nth-child(1)", $(this)).text().trim(), localname: $("td:nth-child(2)", $(this)).text().trim()})});langs.sort((a, b) => a.name.localeCompare(b.name))
 */
export const languages = [
    {
        code: "ab",
        name: "Abkhazian",
        localname: "Аҧсшәа"
    },
    {
        code: "ace",
        name: "Acehnese",
        localname: "Acèh"
    },
    {
        code: "ady",
        name: "Adyghe",
        localname: "адыгабзэ"
    },
    {
        code: "aa",
        name: "Afar",
        localname: "Qafár af"
    },
    {
        code: "af",
        name: "Afrikaans",
        localname: "Afrikaans"
    },
    {
        code: "ak",
        name: "Akan",
        localname: "Akan"
    },
    {
        code: "sq",
        name: "Albanian",
        localname: "shqip"
    },
    {
        code: "als",
        name: "Alemannic",
        localname: "Alemannisch"
    },
    {
        code: "am",
        name: "Amharic",
        localname: "አማርኛ"
    },
    {
        code: "ang",
        name: "Anglo-Saxon",
        localname: "Ænglisc"
    },
    {
        code: "ar",
        name: "Arabic",
        localname: "العربية"
    },
    {
        code: "an",
        name: "Aragonese",
        localname: "aragonés"
    },
    {
        code: "arc",
        name: "Aramaic",
        localname: "ܐܪܡܝܐ"
    },
    {
        code: "hy",
        name: "Armenian",
        localname: "հայերեն"
    },
    {
        code: "roa-rup",
        name: "Aromanian",
        localname: "armãneashti"
    },
    {
        code: "as",
        name: "Assamese",
        localname: "অসমীয়া"
    },
    {
        code: "ast",
        name: "Asturian",
        localname: "asturianu"
    },
    {
        code: "atj",
        name: "Atikamekw",
        localname: "Atikamekw"
    },
    {
        code: "av",
        name: "Avar",
        localname: "авар"
    },
    {
        code: "ay",
        name: "Aymara",
        localname: "Aymar aru"
    },
    {
        code: "az",
        name: "Azerbaijani",
        localname: "azərbaycanca"
    },
    {
        code: "bm",
        name: "Bambara",
        localname: "bamanankan"
    },
    {
        code: "bjn",
        name: "Banjar",
        localname: "Bahasa Banjar"
    },
    {
        code: "map-bms",
        name: "Banyumasan",
        localname: "Basa Banyumasan"
    },
    {
        code: "ba",
        name: "Bashkir",
        localname: "башҡортса"
    },
    {
        code: "eu",
        name: "Basque",
        localname: "euskara"
    },
    {
        code: "bar",
        name: "Bavarian",
        localname: "Boarisch"
    },
    {
        code: "be",
        name: "Belarusian",
        localname: "беларуская"
    },
    {
        code: "be-tarask",
        name: "Belarusian (Taraškievica)",
        localname: "беларуская (тарашкевіца)‎"
    },
    {
        code: "bn",
        name: "Bengali",
        localname: "বাংলা"
    },
    {
        code: "bh",
        name: "Bhojpuri",
        localname: "भोजपुरी"
    },
    {
        code: "bpy",
        name: "Bishnupriya Manipuri",
        localname: "বিষ্ণুপ্রিয়া মণিপুরী"
    },
    {
        code: "bi",
        name: "Bislama",
        localname: "Bislama"
    },
    {
        code: "bs",
        name: "Bosnian",
        localname: "bosanski"
    },
    {
        code: "br",
        name: "Breton",
        localname: "brezhoneg"
    },
    {
        code: "bug",
        name: "Buginese",
        localname: "ᨅᨔ ᨕᨘᨁᨗ"
    },
    {
        code: "bg",
        name: "Bulgarian",
        localname: "български"
    },
    {
        code: "my",
        name: "Burmese",
        localname: "မြန်မာဘာသာ"
    },
    {
        code: "bxr",
        name: "Buryat",
        localname: "буряад"
    },
    {
        code: "zh-yue",
        name: "Cantonese",
        localname: "粵語"
    },
    {
        code: "ca",
        name: "Catalan",
        localname: "català"
    },
    {
        code: "ceb",
        name: "Cebuano",
        localname: "Cebuano"
    },
    {
        code: "bcl",
        name: "Central Bicolano",
        localname: "Bikol Central"
    },
    {
        code: "ch",
        name: "Chamorro",
        localname: "Chamoru"
    },
    {
        code: "cbk-zam",
        name: "Chavacano",
        localname: "Chavacano de Zamboanga"
    },
    {
        code: "ce",
        name: "Chechen",
        localname: "нохчийн"
    },
    {
        code: "chr",
        name: "Cherokee",
        localname: "ᏣᎳᎩ"
    },
    {
        code: "chy",
        name: "Cheyenne",
        localname: "Tsetsêhestâhese"
    },
    {
        code: "ny",
        name: "Chichewa",
        localname: "Chi-Chewa"
    },
    {
        code: "zh",
        name: "Chinese",
        localname: "中文"
    },
    {
        code: "cho",
        name: "Choctaw",
        localname: "Choctaw"
    },
    {
        code: "cv",
        name: "Chuvash",
        localname: "Чӑвашла"
    },
    {
        code: "zh-classical",
        name: "Classical Chinese",
        localname: "文言"
    },
    {
        code: "kw",
        name: "Cornish",
        localname: "kernowek"
    },
    {
        code: "co",
        name: "Corsican",
        localname: "corsu"
    },
    {
        code: "cr",
        name: "Cree",
        localname: "Nēhiyawēwin / ᓀᐦᐃᔭᐍᐏᐣ"
    },
    {
        code: "crh",
        name: "Crimean Tatar",
        localname: "qırımtatarca"
    },
    {
        code: "hr",
        name: "Croatian",
        localname: "hrvatski"
    },
    {
        code: "cs",
        name: "Czech",
        localname: "čeština"
    },
    {
        code: "da",
        name: "Danish",
        localname: "dansk"
    },
    {
        code: "din",
        name: "Dinka",
        localname: "Thuɔŋjäŋ"
    },
    {
        code: "dv",
        name: "Divehi",
        localname: "ދިވެހިބަސް"
    },
    {
        code: "dty",
        name: "Doteli",
        localname: "डोटेली"
    },
    {
        code: "nl",
        name: "Dutch",
        localname: "Nederlands"
    },
    {
        code: "nds-nl",
        name: "Dutch Low Saxon",
        localname: "Nedersaksies"
    },
    {
        code: "dz",
        name: "Dzongkha",
        localname: "ཇོང་ཁ"
    },
    {
        code: "pa",
        name: "Eastern Punjabi",
        localname: "ਪੰਜਾਬੀ"
    },
    {
        code: "arz",
        name: "Egyptian Arabic",
        localname: "مصرى"
    },
    {
        code: "eml",
        name: "Emilian-Romagnol",
        localname: "emiliàn e rumagnòl"
    },
    {
        code: "en",
        name: "English",
        localname: "English"
    },
    {
        code: "myv",
        name: "Erzya",
        localname: "эрзянь"
    },
    {
        code: "eo",
        name: "Esperanto",
        localname: "Esperanto"
    },
    {
        code: "et",
        name: "Estonian",
        localname: "eesti"
    },
    {
        code: "ee",
        name: "Ewe",
        localname: "eʋegbe"
    },
    {
        code: "ext",
        name: "Extremaduran",
        localname: "estremeñu"
    },
    {
        code: "fo",
        name: "Faroese",
        localname: "føroyskt"
    },
    {
        code: "hif",
        name: "Fiji Hindi",
        localname: "Fiji Hindi"
    },
    {
        code: "fj",
        name: "Fijian",
        localname: "Na Vosa Vakaviti"
    },
    {
        code: "fi",
        name: "Finnish",
        localname: "suomi"
    },
    {
        code: "frp",
        name: "Franco-Provençal",
        localname: "arpetan"
    },
    {
        code: "fr",
        name: "French",
        localname: "français"
    },
    {
        code: "fur",
        name: "Friulian",
        localname: "furlan"
    },
    {
        code: "ff",
        name: "Fula",
        localname: "Fulfulde"
    },
    {
        code: "gag",
        name: "Gagauz",
        localname: "Gagauz"
    },
    {
        code: "gl",
        name: "Galician",
        localname: "galego"
    },
    {
        code: "gan",
        name: "Gan",
        localname: "贛語"
    },
    {
        code: "ka",
        name: "Georgian",
        localname: "ქართული"
    },
    {
        code: "de",
        name: "German",
        localname: "Deutsch"
    },
    {
        code: "glk",
        name: "Gilaki",
        localname: "گیلکی"
    },
    {
        code: "gor",
        name: "Gorontalo",
        localname: "Bahasa Hulontalo"
    },
    {
        code: "got",
        name: "Gothic",
        localname: "𐌲𐌿𐍄𐌹𐍃𐌺"
    },
    {
        code: "el",
        name: "Greek",
        localname: "Ελληνικά"
    },
    {
        code: "kl",
        name: "Greenlandic",
        localname: "kalaallisut"
    },
    {
        code: "gn",
        name: "Guarani",
        localname: "Avañe'ẽ"
    },
    {
        code: "gu",
        name: "Gujarati",
        localname: "ગુજરાતી"
    },
    {
        code: "ht",
        name: "Haitian",
        localname: "Kreyòl ayisyen"
    },
    {
        code: "hak",
        name: "Hakka",
        localname: "客家語/Hak-kâ-ngî"
    },
    {
        code: "ha",
        name: "Hausa",
        localname: "Hausa"
    },
    {
        code: "haw",
        name: "Hawaiian",
        localname: "Hawaiʻi"
    },
    {
        code: "he",
        name: "Hebrew",
        localname: "עברית"
    },
    {
        code: "hz",
        name: "Herero",
        localname: "Otsiherero"
    },
    {
        code: "mrj",
        name: "Hill Mari",
        localname: "кырык мары"
    },
    {
        code: "hi",
        name: "Hindi",
        localname: "हिन्दी"
    },
    {
        code: "ho",
        name: "Hiri Motu",
        localname: "Hiri Motu"
    },
    {
        code: "hu",
        name: "Hungarian",
        localname: "magyar"
    },
    {
        code: "is",
        name: "Icelandic",
        localname: "íslenska"
    },
    {
        code: "io",
        name: "Ido",
        localname: "Ido"
    },
    {
        code: "ig",
        name: "Igbo",
        localname: "Igbo"
    },
    {
        code: "ilo",
        name: "Ilocano",
        localname: "Ilokano"
    },
    {
        code: "id",
        name: "Indonesian",
        localname: "Bahasa Indonesia"
    },
    {
        code: "inh",
        name: "Ingush",
        localname: "ГӀалгӀай"
    },
    {
        code: "ia",
        name: "Interlingua",
        localname: "interlingua"
    },
    {
        code: "ie",
        name: "Interlingue",
        localname: "Interlingue"
    },
    {
        code: "iu",
        name: "Inuktitut",
        localname: "ᐃᓄᒃᑎᑐᑦ/inuktitut"
    },
    {
        code: "ik",
        name: "Inupiak",
        localname: "Iñupiak"
    },
    {
        code: "ga",
        name: "Irish",
        localname: "Gaeilge"
    },
    {
        code: "it",
        name: "Italian",
        localname: "italiano"
    },
    {
        code: "jam",
        name: "Jamaican Patois",
        localname: "Patois"
    },
    {
        code: "ja",
        name: "Japanese",
        localname: "日本語"
    },
    {
        code: "jv",
        name: "Javanese",
        localname: "Basa Jawa"
    },
    {
        code: "kbd",
        name: "Kabardian",
        localname: "Адыгэбзэ"
    },
    {
        code: "kbp",
        name: "Kabiye",
        localname: "Kabɩyɛ"
    },
    {
        code: "kab",
        name: "Kabyle",
        localname: "Taqbaylit"
    },
    {
        code: "xal",
        name: "Kalmyk",
        localname: "хальмг"
    },
    {
        code: "kn",
        name: "Kannada",
        localname: "ಕನ್ನಡ"
    },
    {
        code: "kr",
        name: "Kanuri",
        localname: "Kanuri"
    },
    {
        code: "pam",
        name: "Kapampangan",
        localname: "Kapampangan"
    },
    {
        code: "krc",
        name: "Karachay-Balkar",
        localname: "къарачай-малкъар"
    },
    {
        code: "kaa",
        name: "Karakalpak",
        localname: "Qaraqalpaqsha"
    },
    {
        code: "ks",
        name: "Kashmiri",
        localname: "कॉशुर / کٲشُر"
    },
    {
        code: "csb",
        name: "Kashubian",
        localname: "kaszëbsczi"
    },
    {
        code: "kk",
        name: "Kazakh",
        localname: "қазақша"
    },
    {
        code: "km",
        name: "Khmer",
        localname: "ភាសាខ្មែរ"
    },
    {
        code: "ki",
        name: "Kikuyu",
        localname: "Gĩkũyũ"
    },
    {
        code: "rw",
        name: "Kinyarwanda",
        localname: "Kinyarwanda"
    },
    {
        code: "ky",
        name: "Kirghiz",
        localname: "Кыргызча"
    },
    {
        code: "rn",
        name: "Kirundi",
        localname: "Kirundi"
    },
    {
        code: "kv",
        name: "Komi",
        localname: "коми"
    },
    {
        code: "koi",
        name: "Komi-Permyak",
        localname: "Перем Коми"
    },
    {
        code: "kg",
        name: "Kongo",
        localname: "Kongo"
    },
    {
        code: "gom",
        name: "Konkani",
        localname: "गोंयची कोंकणी / Gõychi Konknni"
    },
    {
        code: "ko",
        name: "Korean",
        localname: "한국어"
    },
    {
        code: "kj",
        name: "Kuanyama",
        localname: "Kwanyama"
    },
    {
        code: "ku",
        name: "Kurdish (Kurmanji)",
        localname: "kurdî"
    },
    {
        code: "ckb",
        name: "Kurdish (Sorani)",
        localname: "کوردی"
    },
    {
        code: "lad",
        name: "Ladino",
        localname: "Ladino"
    },
    {
        code: "lbe",
        name: "Lak",
        localname: "лакку"
    },
    {
        code: "lo",
        name: "Lao",
        localname: "ລາວ"
    },
    {
        code: "ltg",
        name: "Latgalian",
        localname: "latgaļu"
    },
    {
        code: "la",
        name: "Latin",
        localname: "Latina"
    },
    {
        code: "lv",
        name: "Latvian",
        localname: "latviešu"
    },
    {
        code: "lez",
        name: "Lezgian",
        localname: "лезги"
    },
    {
        code: "lij",
        name: "Ligurian",
        localname: "Ligure"
    },
    {
        code: "li",
        name: "Limburgish",
        localname: "Limburgs"
    },
    {
        code: "ln",
        name: "Lingala",
        localname: "lingála"
    },
    {
        code: "lfn",
        name: "Lingua Franca Nova",
        localname: "Lingua Franca Nova"
    },
    {
        code: "lt",
        name: "Lithuanian",
        localname: "lietuvių"
    },
    {
        code: "olo",
        name: "Livvi-Karelian",
        localname: "Livvinkarjala"
    },
    {
        code: "jbo",
        name: "Lojban",
        localname: "la .lojban."
    },
    {
        code: "lmo",
        name: "Lombard",
        localname: "lumbaart"
    },
    {
        code: "nds",
        name: "Low Saxon",
        localname: "Plattdüütsch"
    },
    {
        code: "dsb",
        name: "Lower Sorbian",
        localname: "dolnoserbski"
    },
    {
        code: "lg",
        name: "Luganda",
        localname: "Luganda"
    },
    {
        code: "lb",
        name: "Luxembourgish",
        localname: "Lëtzebuergesch"
    },
    {
        code: "mk",
        name: "Macedonian",
        localname: "македонски"
    },
    {
        code: "mai",
        name: "Maithili",
        localname: "मैथिली"
    },
    {
        code: "mg",
        name: "Malagasy",
        localname: "Malagasy"
    },
    {
        code: "ms",
        name: "Malay",
        localname: "Bahasa Melayu"
    },
    {
        code: "ml",
        name: "Malayalam",
        localname: "മലയാളം"
    },
    {
        code: "mt",
        name: "Maltese",
        localname: "Malti"
    },
    {
        code: "gv",
        name: "Manx",
        localname: "Gaelg"
    },
    {
        code: "mi",
        name: "Maori",
        localname: "Māori"
    },
    {
        code: "mr",
        name: "Marathi",
        localname: "मराठी"
    },
    {
        code: "mh",
        name: "Marshallese",
        localname: "Ebon"
    },
    {
        code: "mzn",
        name: "Mazandarani",
        localname: "مازِرونی"
    },
    {
        code: "mhr",
        name: "Meadow Mari",
        localname: "олык марий"
    },
    {
        code: "cdo",
        name: "Min Dong",
        localname: "Mìng-dĕ̤ng-ngṳ̄"
    },
    {
        code: "zh-min-nan",
        name: "Min Nan",
        localname: "Bân-lâm-gú"
    },
    {
        code: "min",
        name: "Minangkabau",
        localname: "Baso Minangkabau"
    },
    {
        code: "xmf",
        name: "Mingrelian",
        localname: "მარგალური"
    },
    {
        code: "mwl",
        name: "Mirandese",
        localname: "Mirandés"
    },
    {
        code: "mdf",
        name: "Moksha",
        localname: "мокшень"
    },
    {
        code: "mn",
        name: "Mongolian",
        localname: "монгол"
    },
    {
        code: "mus",
        name: "Muscogee",
        localname: "Mvskoke"
    },
    {
        code: "nah",
        name: "Nahuatl",
        localname: "Nāhuatl"
    },
    {
        code: "na",
        name: "Nauruan",
        localname: "Dorerin Naoero"
    },
    {
        code: "nv",
        name: "Navajo",
        localname: "Diné bizaad"
    },
    {
        code: "ng",
        name: "Ndonga",
        localname: "Oshiwambo"
    },
    {
        code: "nap",
        name: "Neapolitan",
        localname: "Napulitano"
    },
    {
        code: "ne",
        name: "Nepali",
        localname: "नेपाली"
    },
    {
        code: "new",
        name: "Newar",
        localname: "नेपाल भाषा"
    },
    {
        code: "pih",
        name: "Norfolk",
        localname: "Norfuk / Pitkern"
    },
    {
        code: "nrm",
        name: "Norman",
        localname: "Nouormand"
    },
    {
        code: "frr",
        name: "North Frisian",
        localname: "Nordfriisk"
    },
    {
        code: "lrc",
        name: "Northern Luri",
        localname: "لۊری شومالی"
    },
    {
        code: "se",
        name: "Northern Sami",
        localname: "davvisámegiella"
    },
    {
        code: "nso",
        name: "Northern Sotho",
        localname: "Sesotho sa Leboa"
    },
    {
        code: "no",
        name: "Norwegian (Bokmål)",
        localname: "norsk"
    },
    {
        code: "nn",
        name: "Norwegian (Nynorsk)",
        localname: "norsk nynorsk"
    },
    {
        code: "nov",
        name: "Novial",
        localname: "Novial"
    },
    {
        code: "ii",
        name: "Nuosu",
        localname: "ꆇꉙ"
    },
    {
        code: "oc",
        name: "Occitan",
        localname: "occitan"
    },
    {
        code: "or",
        name: "Odia",
        localname: "ଓଡ଼ିଆ"
    },
    {
        code: "cu",
        name: "Old Church Slavonic",
        localname: "словѣньскъ / ⰔⰎⰑⰂⰡⰐⰠⰔⰍⰟ"
    },
    {
        code: "om",
        name: "Oromo",
        localname: "Oromoo"
    },
    {
        code: "os",
        name: "Ossetian",
        localname: "Ирон"
    },
    {
        code: "pfl",
        name: "Palatinate German",
        localname: "Pälzisch"
    },
    {
        code: "pi",
        name: "Pali",
        localname: "पालि"
    },
    {
        code: "pag",
        name: "Pangasinan",
        localname: "Pangasinan"
    },
    {
        code: "pap",
        name: "Papiamentu",
        localname: "Papiamentu"
    },
    {
        code: "ps",
        name: "Pashto",
        localname: "پښتو"
    },
    {
        code: "pdc",
        name: "Pennsylvania German",
        localname: "Deitsch"
    },
    {
        code: "fa",
        name: "Persian",
        localname: "فارسی"
    },
    {
        code: "pcd",
        name: "Picard",
        localname: "Picard"
    },
    {
        code: "pms",
        name: "Piedmontese",
        localname: "Piemontèis"
    },
    {
        code: "pl",
        name: "Polish",
        localname: "polski"
    },
    {
        code: "pnt",
        name: "Pontic",
        localname: "Ποντιακά"
    },
    {
        code: "pt",
        name: "Portuguese",
        localname: "português"
    },
    {
        code: "qu",
        name: "Quechua",
        localname: "Runa Simi"
    },
    {
        code: "ksh",
        name: "Ripuarian",
        localname: "Ripoarisch"
    },
    {
        code: "rmy",
        name: "Romani",
        localname: "Romani"
    },
    {
        code: "ro",
        name: "Romanian",
        localname: "română"
    },
    {
        code: "rm",
        name: "Romansh",
        localname: "rumantsch"
    },
    {
        code: "ru",
        name: "Russian",
        localname: "русский"
    },
    {
        code: "rue",
        name: "Rusyn",
        localname: "русиньскый"
    },
    {
        code: "sah",
        name: "Sakha",
        localname: "саха тыла"
    },
    {
        code: "sm",
        name: "Samoan",
        localname: "Gagana Samoa"
    },
    {
        code: "bat-smg",
        name: "Samogitian",
        localname: "žemaitėška"
    },
    {
        code: "sg",
        name: "Sango",
        localname: "Sängö"
    },
    {
        code: "sa",
        name: "Sanskrit",
        localname: "संस्कृतम्"
    },
    {
        code: "sat",
        name: "Santali",
        localname: "ᱥᱟᱱᱛᱟᱲᱤ"
    },
    {
        code: "sc",
        name: "Sardinian",
        localname: "sardu"
    },
    {
        code: "stq",
        name: "Saterland Frisian",
        localname: "Seeltersk"
    },
    {
        code: "sco",
        name: "Scots",
        localname: "Scots"
    },
    {
        code: "gd",
        name: "Scottish Gaelic",
        localname: "Gàidhlig"
    },
    {
        code: "sr",
        name: "Serbian",
        localname: "српски / srpski"
    },
    {
        code: "sh",
        name: "Serbo-Croatian",
        localname: "srpskohrvatski / српскохрватски"
    },
    {
        code: "st",
        name: "Sesotho",
        localname: "Sesotho"
    },
    {
        code: "sn",
        name: "Shona",
        localname: "chiShona"
    },
    {
        code: "scn",
        name: "Sicilian",
        localname: "sicilianu"
    },
    {
        code: "szl",
        name: "Silesian",
        localname: "ślůnski"
    },
    {
        code: "simple",
        name: "Simple English",
        localname: "Simple English"
    },
    {
        code: "sd",
        name: "Sindhi",
        localname: "سنڌي"
    },
    {
        code: "si",
        name: "Sinhalese",
        localname: "සිංහල"
    },
    {
        code: "sk",
        name: "Slovak",
        localname: "slovenčina"
    },
    {
        code: "sl",
        name: "Slovenian",
        localname: "slovenščina"
    },
    {
        code: "so",
        name: "Somali",
        localname: "Soomaaliga"
    },
    {
        code: "azb",
        name: "Southern Azerbaijani",
        localname: "تۆرکجه"
    },
    {
        code: "es",
        name: "Spanish",
        localname: "español"
    },
    {
        code: "srn",
        name: "Sranan",
        localname: "Sranantongo"
    },
    {
        code: "su",
        name: "Sundanese",
        localname: "Basa Sunda"
    },
    {
        code: "sw",
        name: "Swahili",
        localname: "Kiswahili"
    },
    {
        code: "ss",
        name: "Swati",
        localname: "SiSwati"
    },
    {
        code: "sv",
        name: "Swedish",
        localname: "svenska"
    },
    {
        code: "tl",
        name: "Tagalog",
        localname: "Tagalog"
    },
    {
        code: "ty",
        name: "Tahitian",
        localname: "reo tahiti"
    },
    {
        code: "tg",
        name: "Tajik",
        localname: "тоҷикӣ"
    },
    {
        code: "ta",
        name: "Tamil",
        localname: "தமிழ்"
    },
    {
        code: "roa-tara",
        name: "Tarantino",
        localname: "tarandíne"
    },
    {
        code: "tt",
        name: "Tatar",
        localname: "татарча/tatarça"
    },
    {
        code: "te",
        name: "Telugu",
        localname: "తెలుగు"
    },
    {
        code: "tet",
        name: "Tetum",
        localname: "tetun"
    },
    {
        code: "th",
        name: "Thai",
        localname: "ไทย"
    },
    {
        code: "bo",
        name: "Tibetan",
        localname: "བོད་ཡིག"
    },
    {
        code: "ti",
        name: "Tigrinya",
        localname: "ትግርኛ"
    },
    {
        code: "tpi",
        name: "Tok Pisin",
        localname: "Tok Pisin"
    },
    {
        code: "to",
        name: "Tongan",
        localname: "lea faka-Tonga"
    },
    {
        code: "ts",
        name: "Tsonga",
        localname: "Xitsonga"
    },
    {
        code: "tn",
        name: "Tswana",
        localname: "Setswana"
    },
    {
        code: "tcy",
        name: "Tulu",
        localname: "ತುಳು"
    },
    {
        code: "tum",
        name: "Tumbuka",
        localname: "chiTumbuka"
    },
    {
        code: "tr",
        name: "Turkish",
        localname: "Türkçe"
    },
    {
        code: "tk",
        name: "Turkmen",
        localname: "Türkmençe"
    },
    {
        code: "tyv",
        name: "Tuvan",
        localname: "тыва дыл"
    },
    {
        code: "tw",
        name: "Twi",
        localname: "Twi"
    },
    {
        code: "udm",
        name: "Udmurt",
        localname: "удмурт"
    },
    {
        code: "uk",
        name: "Ukrainian",
        localname: "українська"
    },
    {
        code: "hsb",
        name: "Upper Sorbian",
        localname: "hornjoserbsce"
    },
    {
        code: "ur",
        name: "Urdu",
        localname: "اردو"
    },
    {
        code: "ug",
        name: "Uyghur",
        localname: "ئۇيغۇرچە / Uyghurche"
    },
    {
        code: "uz",
        name: "Uzbek",
        localname: "oʻzbekcha/ўзбекча"
    },
    {
        code: "ve",
        name: "Venda",
        localname: "Tshivenda"
    },
    {
        code: "vec",
        name: "Venetian",
        localname: "vèneto"
    },
    {
        code: "vep",
        name: "Vepsian",
        localname: "vepsän kel’"
    },
    {
        code: "vi",
        name: "Vietnamese",
        localname: "Tiếng Việt"
    },
    {
        code: "vo",
        name: "Volapük",
        localname: "Volapük"
    },
    {
        code: "fiu-vro",
        name: "Võro",
        localname: "Võro"
    },
    {
        code: "wa",
        name: "Walloon",
        localname: "walon"
    },
    {
        code: "war",
        name: "Waray",
        localname: "Winaray"
    },
    {
        code: "cy",
        name: "Welsh",
        localname: "Cymraeg"
    },
    {
        code: "vls",
        name: "West Flemish",
        localname: "West-Vlams"
    },
    {
        code: "fy",
        name: "West Frisian",
        localname: "Frysk"
    },
    {
        code: "pnb",
        name: "Western Punjabi",
        localname: "پنجابی"
    },
    {
        code: "wo",
        name: "Wolof",
        localname: "Wolof"
    },
    {
        code: "wuu",
        name: "Wu",
        localname: "吴语"
    },
    {
        code: "xh",
        name: "Xhosa",
        localname: "isiXhosa"
    },
    {
        code: "yi",
        name: "Yiddish",
        localname: "ייִדיש"
    },
    {
        code: "yo",
        name: "Yoruba",
        localname: "Yorùbá"
    },
    {
        code: "diq",
        name: "Zazaki",
        localname: "Zazaki"
    },
    {
        code: "zea",
        name: "Zeelandic",
        localname: "Zeêuws"
    },
    {
        code: "za",
        name: "Zhuang",
        localname: "Vahcuengh"
    },
    {
        code: "zu",
        name: "Zulu",
        localname: "isiZulu"
    }
]