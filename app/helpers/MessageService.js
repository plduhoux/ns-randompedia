/**
 * A message contains : 
 *  - id : unique identifier of this kind of message
 *  - onlyonce : display it only the first time it is fired, then ignore displays (use localStorage)
 *  - replaceExisting : replace another message with same id by this one
 *  - icon : a font-awesome icon code starting with fa-
 *  - text : The message to display
 *  - bgcolor : color of the background of the message
 * 
 * Messages are displayed by the Messages component. Anyone can push messages through this service
 */

import localStorage from "nativescript-localstorage/localstorage"

class MessageService {
    constructor() {
        this.messages = []
    }
    display(message) {
        if (message.onlyonce && localStorage.getItem("message-" + message.id)) {
            return // message already displayed once
        }
        if (this.messages.find(mess => mess.id === message.id) !== undefined) {
            if (message.replaceExisting) {
                //remove existing one
                this.messages.splice(this.messages.findIndex(i => i.id === message.id), 1)
            } else {
                return // already this message to display
            }
        }
        this.messages.push(message)
        localStorage.setItem("message-" + message.id, "done")
    }
    disable(message) {
        this.messages.splice(this.messages.findIndex(i => i.id === message.id), 1)
    }
}
export default new MessageService()