export function getWikipediaHostUrl(language) {
    return "https://" + language + ".wikipedia.org"
}

export function getWikipediaBaseUrl(language) {
    return getWikipediaHostUrl(language) + "/wiki/"
}

export function getWikipediaRandomUrl(language) {
    return getWikipediaBaseUrl(language) + "Special:Random"
}
