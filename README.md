# Random Pedia

**Improve your general knowledge learning random things**

Random Pedia is a mobile app which displays multiple pages from Wikipedia chosen randomly.
You can : 
 - preview title, extract and pictures from the wikipedia article
 - reload all random pages,
 - reload a page (swipe the item to the left),
 - save pages you like (swipe the item to the right and click on the save button)
 - access the real wikipedia page for an entry (tap on the item to view full extract and tap on "Read more...")
 - access your saved pages (by tapping on the archive box)
 - choose the language in which the articles are loaded and the number of items to display

## Download
You can download it for Android from Google Play store : 
https://play.google.com/store/apps/details?id=fr.plduhoux.randompedia

It is not yet available in the Apple Store but you can build it following Usage instructions

## Usage

``` bash
# Install dependencies
npm install

# Build for production
tns build <platform> --bundle

# Build, watch for changes and debug the application
tns debug <platform> --bundle

# Build, watch for changes and run the application
tns run <platform> --bundle
```
